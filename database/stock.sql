PGDMP                         x            postgres    12.3    12.3                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    13318    postgres    DATABASE     �   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE postgres;
                postgres    false            	           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    2824                        3079    16384 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                   false            
           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                        false    1            �            1259    16401    stocks    TABLE     �   CREATE TABLE public.stocks (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    unit_price double precision NOT NULL,
    quantity integer NOT NULL,
    import_date character varying(20)
);
    DROP TABLE public.stocks;
       public         heap    postgres    false            �            1259    16404    tblstock_stoid_seq    SEQUENCE     {   CREATE SEQUENCE public.tblstock_stoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.tblstock_stoid_seq;
       public          postgres    false    203                       0    0    tblstock_stoid_seq    SEQUENCE OWNED BY     D   ALTER SEQUENCE public.tblstock_stoid_seq OWNED BY public.stocks.id;
          public          postgres    false    204            �
           2604    16406 	   stocks id    DEFAULT     k   ALTER TABLE ONLY public.stocks ALTER COLUMN id SET DEFAULT nextval('public.tblstock_stoid_seq'::regclass);
 8   ALTER TABLE public.stocks ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    203                      0    16401    stocks 
   TABLE DATA           M   COPY public.stocks (id, name, unit_price, quantity, import_date) FROM stdin;
    public          postgres    false    203   j                  0    0    tblstock_stoid_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.tblstock_stoid_seq', 73, true);
          public          postgres    false    204            �
           2606    16408    stocks tblstock_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.stocks
    ADD CONSTRAINT tblstock_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.stocks DROP CONSTRAINT tblstock_pkey;
       public            postgres    false    203               �   x�}�;
1E�:YŬ�I���L;n�F,D�'V�����}�^n�経���Z�ڴ�Vm>�|����Y�y��q_Ο��l��m�vh�Q���S�IQɧP'!#!#!#!#!#!#!#!#!#!#!'!'!'!'!'!'!'!'!'!'� � � � � � � � � � �$�$�$�$�$��
-��jQQQ��HD$"��D'	��DB"�AB��	��j�o	��_     