
package stockmanagementconsole.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import stockmanagementconsole.configuration.DataBaseConnection;
import stockmanagementconsole.model.Stock;
import stockmanagementconsole.view.TableConsole;

public class DisplayController
{
	
	TableConsole table = new TableConsole();

	PreparedStatement prepare_statement;
	ResultSet result_set;
	Connection connection = new DataBaseConnection().getConnection();
	Stock stock;
	static int limit = 5;
	static int offset = 0;
	static int rows = 5;
	
	public void index()
	{
		table.index(getStockRecord());
	}

	public ArrayList<Stock> getStockRecord()
	{
		ArrayList<Stock> arr_stock = new ArrayList<>();
		String sqlCommand = "Select * from stocks  Order By id ASC Limit ? Offset ?";
		try
		{
			prepare_statement = connection.prepareStatement(sqlCommand);
			prepare_statement.setInt(1, rows);
			prepare_statement.setInt(2, offset);
			result_set = prepare_statement.executeQuery();
			while(result_set.next())
			{
				stock = new Stock();
				stock.setId(result_set.getInt("id"));
				stock.setName(result_set.getString("name"));
				stock.setUnitPrice(result_set.getDouble("unit_price"));
				stock.setQuantity(result_set.getInt("quantity"));
				stock.setImportDate(result_set.getString("import_date"));

				arr_stock.add(stock);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				result_set.close();
				prepare_statement.close();
				connection.close();
			} catch (SQLException ex) {
				Logger.getLogger(DisplayController.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return arr_stock;
	}
}
