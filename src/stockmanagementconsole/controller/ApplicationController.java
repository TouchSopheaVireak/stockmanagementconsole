package stockmanagementconsole.controller;

public class ApplicationController
{

	HeaderController header = new HeaderController();
	MenuController menu = new MenuController();
	public void startApplication()
	{
		header.index();
		menu.index();
	}
	
}
