
package stockmanagementconsole.controller;

import stockmanagementconsole.view.SetDataBaseConnection;

public class HeaderController
{
	SetDataBaseConnection set_connection = new SetDataBaseConnection();

	public void index()
	{
		System.out.println("\t\t\t\t==========================");
		System.out.println("\t\t\t\t Stock Management System");
		System.out.println("\t\t\t\t==========================");
		set_connection.checkDataBaseFile();
	}
}
