
package stockmanagementconsole.controller;

import stockmanagementconsole.view.ExitConsole;
import stockmanagementconsole.view.MenuConsole;

public class MenuController
{
	DisplayController display = new DisplayController();
	MenuConsole menu = new MenuConsole();
	ExitConsole exit = new ExitConsole();
	
	private String option;

	public void index()
	{
		option = menu.makeMenu().toLowerCase();
		do {
			switch(option)
			{
				case "*":
					display.index();
					break;
				case "e":
					exit.stopApp();
					break;
			}
		} while(!option.equals("e"));
		System.err.println(option);
	}
	
}
