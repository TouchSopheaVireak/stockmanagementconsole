
package stockmanagementconsole.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBaseConnection
{
	DataBaseConnectionReader configure = new DataBaseConnectionReader();
	Connection connection;

	public Connection getConnection()
	{
		configure.readConfigure();
		String conString = "jdbc:postgresql://localhost:"+configure.Configuration().getPort_number()+"/"+configure.Configuration().getDatabase_name();
		String userName = configure.Configuration().getUsername();
		String Password = configure.Configuration().getPassword();
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(conString,userName,Password);
		}
		catch (ClassNotFoundException | SQLException ex) {
			Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
		}
		return connection;
	}
}
