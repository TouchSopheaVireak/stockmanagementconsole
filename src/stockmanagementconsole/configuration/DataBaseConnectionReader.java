
package stockmanagementconsole.configuration;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBaseConnectionReader
{

	Database configuration;
	ObjectOutputStream obj_stream_out = null;
	ObjectInputStream obj_stream_in = null;

	public void readConfigure()
	{
		try {
			File config = new File("setting"+File.separator+"property.dk");
			obj_stream_in = new ObjectInputStream (new BufferedInputStream (new FileInputStream(config)));
			configuration = (Database) obj_stream_in.readObject();
		} catch (IOException ex) {
			Logger.getLogger(DataBaseConnectionReader.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DataBaseConnectionReader.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void saveConfigure(Database configurationConnection)
	{
		configuration = configurationConnection;
		try {
			boolean dir = new File("setting").mkdir();
			File config = new File("setting"+File.separator+"property.dk");
			obj_stream_out = new ObjectOutputStream (new BufferedOutputStream(new FileOutputStream(config)));
			obj_stream_out.writeObject(configurationConnection);
			obj_stream_out.flush();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(DataBaseConnectionReader.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(DataBaseConnectionReader.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				obj_stream_out.close();
			} catch (IOException ex) {
				Logger.getLogger(DataBaseConnectionReader.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public Database Configuration()
	{
		return configuration;
	}
}
