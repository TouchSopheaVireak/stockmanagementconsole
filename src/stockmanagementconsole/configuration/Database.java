
package stockmanagementconsole.configuration;

import java.io.Serializable;
public class Database implements Serializable
{

	private String username;
	private String password;
	private String database_name;
	private int port_number;
	
	public int getPort_number()
	{
		return port_number;
	}

	public void setPort_number(int port_number)
	{
		this.port_number = port_number;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getDatabase_name() {
		return database_name;
	}

	public void setDatabase_name(String database_name)
	{
		this.database_name = database_name;
	}
}
