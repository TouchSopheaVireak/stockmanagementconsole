
package stockmanagementconsole.model;
import java.io.Serializable;

public class Stock implements Serializable
{
	private long id;
	private String name;
	private double unit_price;
	private int quantity;
	private String import_date;

//	public Stock(int id, String name, double unit_price, int quantity, String import_date)
//	{
//		this.id = id;
//		this.name = name;
//		this.unit_price = unit_price;
//		this.quantity = quantity;
//		this.import_date = import_date;
//	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public double getUnitPrice()
	{
		return unit_price;
	}

	public void setUnitPrice(double unit_price)
	{
		this.unit_price = unit_price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getImportDate()
	{
		return import_date;
	}

	public void setImportDate(String import_date) {
		this.import_date = import_date;
	}

}
