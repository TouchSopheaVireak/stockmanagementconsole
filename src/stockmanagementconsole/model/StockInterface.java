
package stockmanagementconsole.model;

import java.util.ArrayList;
public interface StockInterface
{
	public void insert(Stock stock);
	public ArrayList<Stock> getAllStock();
	public void update(Stock pro);
	public void delete(int id);
	public Stock search(int id);
}
