
package stockmanagementconsole.view;

import java.util.Scanner;

public class ExitConsole
{
	public boolean exitDecision()
	{
		String option;
		boolean result = false;
		Scanner scan = new Scanner(System.in);
		System.out.print("Are you sure to exit?[y/n] : ");
		option = scan.nextLine().toLowerCase();
		if ("y".equals(option)) {
			result = true;
		}
		return result;
	}

	public void stopApp()
	{
		if(exitDecision())
		{
			System.exit(0);
		}
	}
}
