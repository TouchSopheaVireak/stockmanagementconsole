
package stockmanagementconsole.view;

import java.io.*;
import java.util.Scanner;
import stockmanagementconsole.configuration.DataBaseConnectionReader;
import stockmanagementconsole.configuration.Database;

public class SetDataBaseConnection
{
	Scanner scan = new Scanner(System.in);
	DataBaseConnectionReader connection_file = new DataBaseConnectionReader();
	Database database_connection;
	ObjectInputStream obj_stream_in = null;

	public void checkDataBaseFile()
	{
		try {
			File config = new File("setting"+File.separator+"property.dk");
			obj_stream_in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(config)));
		} catch (IOException ex) {
			createDataBaseConfigure();
		}
	}

	public void createDataBaseConfigure()
	{
		database_connection = new Database();
		System.out.print("Enter Port: ");
		database_connection.setPort_number(scan.nextInt());
		System.out.print("\nEnter Database Name: ");
		scan.nextLine();
		database_connection.setDatabase_name(scan.nextLine());
		System.out.print("\nEnter User name: ");
		database_connection.setUsername(scan.nextLine());
		System.out.print("\nEnter Password: ");
		database_connection.setPassword(scan.nextLine());
		connection_file.saveConfigure(database_connection);
	}
}
