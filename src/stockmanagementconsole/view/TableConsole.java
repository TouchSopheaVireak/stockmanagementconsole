
package stockmanagementconsole.view;
import java.util.ArrayList;
import stockmanagementconsole.model.Stock;

public class TableConsole
{

	public void index(ArrayList<Stock> stock)
	{
		table(stock);
		System.out.format("| Page: %-10d/%-10d                                 Total Record: %-10d   |%n");
		System.out.format("+----------------------------------------------------------------------------------------+%n");
	}

	private void table(ArrayList<Stock> stock)
	{
		String leftAlignFormat = "| %-10d | %-17s | %-12.2f $ |  %-13d  |  %-16s  |%n";
		System.out.format("+------------+-------------------+----------------+-----------------+--------------------+%n");
		System.out.format("|     ID     |         Name      |  Unit Price    |  Stock Quantity |    Imported Date   |%n");
		System.out.format("+------------+-------------------+----------------+-----------------+--------------------+%n");
		for (long i =  stock.size()-1 ; i >= 0; i--)
		{
			System.out.format(
					leftAlignFormat,
					stock.get((int) i).getId(),
					stock.get((int) i).getName(),
					stock.get((int) i).getUnitPrice(),
					stock.get((int) i).getQuantity(),
					stock.get((int) i).getImportDate()
			);
			System.out.format("+------------+-------------------+----------------+-----------------+--------------------+%n");
		}
	}
}
