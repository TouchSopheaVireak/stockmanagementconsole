
package stockmanagementconsole.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuConsole
{
	Scanner scan = new Scanner(System.in);

	public String makeMenu()
	{
		String option = null;
		try {
			System.out.println("");
			System.out.format("+----------------------------------------------------------------------------------------+%n");
			System.out.println("|  *)Display | W)write | R)ead | U)pdate | D)elete | F)irst | P)revious | N)ext | L)ast  |");
			System.out.println("|                                                                                        |");
			System.out.println("|  S)earch | G)o to | Se)t row | Ex)port | Im)port | B)ackup | Re)store | H)elp | E)xit  |");
			System.out.format("+----------------------------------------------------------------------------------------+%n");

			System.out.println("");
			System.out.print("Option: ");
			option = scan.nextLine();
		} catch (NumberFormatException e) {
			System.out.println("\n");
			System.err.println("Sorry Invalid Input! Please Try again!");
		}
		return option;
	}
}
