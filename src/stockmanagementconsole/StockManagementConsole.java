
package stockmanagementconsole;
import stockmanagementconsole.controller.ApplicationController;
public class StockManagementConsole
{
    public static void main(String[] args)
    {
        ApplicationController controller = new ApplicationController();
        controller.startApplication();
    }

}
